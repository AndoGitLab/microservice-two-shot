import {useState, useEffect} from 'react';
import {NavLink} from 'react-router-dom';

function ShoesList() {
  const [shoes, setShoes] = useState([])

    const getData = async ()=> {
      const resp = await fetch('http://localhost:8080/api/shoes/')
      const data = await resp.json()

      setShoes(data.shoes)
    }
    useEffect(()=> {
      getData();
    }, [])

    return (
      <div>

        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Bin</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map(shoe => {
              return (
                <tr key={shoe.href}>
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.model_name }</td>
                  <td>{ shoe.color }</td>
                  <td>
                    <img src={shoe.picture_url} width="125" height="100"></img>
                  </td>
                  <td>{ shoe.bin.closet_name }</td>
                  <td><button onClick={async ()=>{
                    const resp = await fetch(`http://localhost:8080/api/shoes/${shoe.id}/`, { method:"DELETE"})
                    const data = await resp.json()
                    getData()
                    }}>Delete</button></td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div>
          <NavLink className="nav-link" aria-current="page" to="new"><button>Add a shoe</button></NavLink>

        </div>
      </div>

      );
    }

export default ShoesList
