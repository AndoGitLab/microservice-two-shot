// import {useState, useEffect,} from 'react';
// import { Link } from 'react-router-dom';


// const initialData = {
//     manufacturer: "",
//     model_name: "",
//     color: "",
//     picture_url: "",
//     bin: "",
// }

// const ShoeForm = () => {
//     const [formData, setFormData] = useState(initialData);
//     const [bins, setBin] = useState([]);


//     const getBins = async () => {
//         const url = await fetch('http://localhost:8100/api/bins/')
//         const data = await url.json()
//         console.log(data.bins)
//         setBin(data.bins)
//     }
//     const handleChange = (e) => {
//         setFormData({
//             ...formData,
//             [e.target.name]:e.target.value
//         })
//     }
//     const handleSubmit = async (e) => {
//         e.preventDefault();
//         const url = `http://localhost:8080${formData.bin}shoes/`;
//         console.log(formData)
//         const fetchOptions = {
//             method: 'post',
//             body: JSON.stringify(formData),
//             headers: {
//                 'Content-Type': 'application/json',
//             },
//         };

//         const resp = await fetch(url, fetchOptions);
//         if (resp.ok) {
//           setFormData(initialData);
//         }
//       }
//     useEffect( () =>{
//         getBins();
//     },[])


//     return (
//         <div className="row">
//     <div className="offset-3 col-6">
//         <div className="shadow p-4 mt-4">
//             <h1>Add New Shoe!</h1>
//             <form onSubmit={handleSubmit} id="create-location-form">
//                 <div className="form-floating mb-3">
//                     <input onChange={handleChange} value={formData.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
//                     <label htmlFor="manufacturer">Manufacturer</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                     <input onChange={handleChange} value={formData.model_name} placeholder="Name" required type="text" name="model_name" id="model_name" className="form-control" />
//                     <label htmlFor="model_name">Model</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                     <input onChange={handleChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
//                     <label htmlFor="color">Color</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                     <input onChange={handleChange} value={formData.picture_url} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
//                     <label htmlFor="picture_url">Picture</label>
//                 </div>
//                 <div className="mb-3">
//                     <select onChange={handleChange} value={formData.bin} required name="bin" id="bin" className="form-select">
//                         <option value="">Choose a bin</option>
//                         {bins.map( bin => {
//                             return (
//                             <option key={bin.href} value={bin.href}>
//                                 {bin.closet_name}
//                             </option>
//                             )
//                         })}
//                     </select>
//                 </div>
//                 <button className="btn btn-primary" >Create a Shoe</button>
//                 {/* <Link to="/shoes"><button className="btn btn-primary" >Create a Shoe</button></Link> */}
//             </form>
//         </div>
//         </div>
//         </div>
//     )


// }
// export default ShoeForm


import React from 'react';
import { NavLink } from 'react-router-dom';



class ShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bin: '',
            bins: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
        this.handleChangeModel = this.handleChangeModel.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangePicture = this.handleChangePicture.bind(this);
        this.handleChangeBin = this.handleChangeBin.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ bins: data.bins });
        }
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.bins;

        const shoeUrl = `http://localhost:8080${data.bin}shoes/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log(shoeUrl)
        console.log(fetchConfig)
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            this.setState({
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bin: '',
            });
        }
        window.location="/shoes"
    }

    handleChangeManufacturer(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value });
    }

    handleChangeModel(event) {
        const value = event.target.value
        this.setState({ model_name: value })
    }

    handleChangeColor(event) {
        const value = event.target.value
        this.setState({ color: value })
    }

    handleChangePicture(event) {
        const value = event.target.value
        this.setState({ picture_url: value })
    }

    handleChangeBin(event) {
        const value = event.target.value
        this.setState({ bin: value })
    }
    redirect(event) {
        <NavLink className="nav-link" aria-current="page" to="/shoes"></NavLink>
    }
    render() {
        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create A New Shoe</h1>
                  <form onSubmit={this.handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                      <input value={this.state.manufacturer} onChange={this.handleChangeManufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                      <label htmlFor="manufacturer">manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input value={this.state.model_name} onChange={this.handleChangeModel} placeholder="model" required type="text" name="model_name" id="model_name" className="form-control"/>
                      <label htmlFor="model_name">model</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input value={this.state.color} onChange={this.handleChangeColor} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                      <label htmlFor="color">color</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input value={this.state.picture_url} onChange={this.handleChangePicture} placeholder="picture" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                      <label htmlFor="picture_url">picture</label>
                    </div>
                    <div className="form-floating mb-3">
                    <select onChange={this.handleChangeBin} value={this.state.bin} required name="bin" id="bin" className="form-select">
                                    <option value="">Choose a bin</option>
                                    {this.state.bins.map( bin => {
                                        return (
                                        <option key={bin.href} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                        )
                                    })}
                                </select>
                    </div>
                    <button className="btn btn-primary">Create</button>

                  </form>
                </div>
              </div>
            </div>
          );
        }
    }

export default ShoeForm
