import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fabric: "",
            style_name: "",
            color: "",
            url: "",
            locations: [],
        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleUrlChange = this.handleUrlChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleCreate = this.handleCreate.bind(this);
    }

    async handleCreate(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        console.log(data);

        const hatsUrl = `http://localhost:8090/api/locations/1/hats/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(hatsUrl, fetchConfig);
        if(response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            const cleared = {
                fabric: "",
                style_name: "",
                color: "",
                url: "",
                location: "",
            };
            this.setState({ success: true });
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/locations/"

        const response = await fetch(url);
        if(response.ok) {
            const data = await response.json();
            console.log(data);
            this.setState({ locations: data.locations });
        }
    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
    }
    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState({ style_name: value });
    }
  }
