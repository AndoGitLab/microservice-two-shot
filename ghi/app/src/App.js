import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import HatList from './HatList';

import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';


function App(props) {
  if (props.shoes === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path="" element={<HatList />} />
            <Route path="" element={<HatForm />} />
          <Route path="/shoes" element={<ShoesList shoes={props.shoes} />} />
          <Route path= "/shoes/new" element={<ShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
