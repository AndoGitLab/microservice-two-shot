from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "href",]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "color", "picture_url", "bin", "id"]

    encoders = {"bin": BinVOEncoder()}



@require_http_methods(["GET", "POST"])
def api_list_shoes(request, id=None):
    if request.method == "GET":
        if id is not None:
            shoes = Shoe.objects.filter(id=id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,)
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            print(bin_href)
            print(BinVO.objects.all())
            bin = BinVO.objects.get(href=bin_href)
            print(bin)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoes(request, id):
    if request.method == "GET":
        shoes = Shoe.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoeListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _= Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            Shoe.objects.filter(id=id).update(**content)
            shoes = Shoe.objects.get(id=id)
            return JsonResponse(
                shoes,
                encoder=ShoeListEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid shoe id"},
                status=400
            )
