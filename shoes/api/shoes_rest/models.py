from django.db import models


class BinVO(models.Model):
    closet_name = models.CharField(max_length=200)
    href = models.CharField(max_length=200, unique=True)



# Create your models here.
class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )
