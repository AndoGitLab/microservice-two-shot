# Wardrobify

Team:

* Anderson - Shoes microservice
* Adrian - Hats microservice?

## Design
Designed different models and created restfulized apis with CRUD functionabilities in order to interact the backend with the frontend. We later used react to create single page applications for better user experience. The front end displayed the list of shoes, had a delete button, and a button to add shoes (aka the form), and on submit the form would direct us back to the shoes list and show the new shoe that was just created.

## Shoes microservice
Anderson-
Made a Shoe model, and BinVO model so that we can add shoes to a particular bin and bins to our wardrobe.



## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
